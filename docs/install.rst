############
Installation
############

``cowboybike`` is compatible with Python 3.4+.

Use :command:`pip` to install the latest stable version of ``cowboybike``:

.. code-block:: console

   $ sudo pip install --upgrade cowboybike

The current development version is available on `gitlab
<https://gitlab.com/samueldumont/python-cowboy-bike>`__. Use :command:`git` and
:command:`python setup.py` to install it:

.. code-block:: console

   $ git clone https://gitlab.com/samueldumont/python-cowboy-bike
   $ cd python-cowboy-bike
   $ sudo python setup.py install
