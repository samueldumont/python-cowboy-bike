import cowboybike
"""
You can auto-discover and run all tests with this command:

    $ pytest

Documentation:

* https://docs.pytest.org/en/latest/
* https://docs.pytest.org/en/latest/fixture.html
* http://flask.pocoo.org/docs/latest/testing/
"""

import pytest
from pathlib import Path
import json
import requests
import requests_mock
from urllib.parse import urljoin
import responses
from datetime import datetime
from datetime import timedelta
import pprint

from .context import cowboybike
from cowboybike import cowboybike

adapter = requests_mock.Adapter()
session = requests.Session()
session.mount('mock', adapter)

COWBOY_URL = "https://app-api.cowboy.bike/"
CHECK_ENDPOINT = "/users/check"
AUTH_ENDPOINT = "/auth/sign_in"
WEATHER_URL = "/weather"
ME_ENDPOINT = "/users/me"
BIKES_ENDPOINT = "/bikes/1"


@responses.activate
def test_user_exists():
    with open('tests/goldenfiles/user-exists.json') as golden:
        responses.add(responses.POST, urljoin(COWBOY_URL, CHECK_ENDPOINT),
                      json=json.loads(golden.read()), status=200)
        assert cowboybike.userExists("user@test.com") == True


@responses.activate
def test_user_does_not_exists():
    with open('tests/goldenfiles/user-not-exists.json') as golden:
        responses.add(responses.POST, urljoin(COWBOY_URL, CHECK_ENDPOINT),
                      json=json.loads(golden.read()), status=404)
        assert cowboybike.userExists("user@test.com") == False


def test_authentication_missing_parameters():
    with pytest.raises(TypeError):
        auth = cowboybike.Authentication()
    with pytest.raises(TypeError):
        auth = cowboybike.Authentication(email="user@test.com")
    with pytest.raises(TypeError):
        auth = cowboybike.Authentication(password="acbdef")


@responses.activate
def test_authentication_existing_parameters():
    with open('tests/goldenfiles/auth.json') as golden:
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Access-Token": "MOCK-ACCESS-TOKEN",
                   "Token-Type": "Bearer",
                   "Client": "MOCK-CLIENT",
                   "Expiry": str((datetime.now() + timedelta(days=365)).timestamp()),
                   "Uid": "user@test.com"}

        responses.add(responses.POST, urljoin(COWBOY_URL, AUTH_ENDPOINT),
                      json=json.loads(golden.read()), status=200, headers=headers)
        auth = cowboybike.Authentication("user@test.com", password="abcdef")
        assert auth.getaccesstoken() == "MOCK-ACCESS-TOKEN"
        assert auth.getclient() == "MOCK-CLIENT"
        assert auth.getuid() == "user@test.com"


@responses.activate
def test_expired_accesstoken():
    with open('tests/goldenfiles/auth.json') as golden:
        with pytest.raises(ValueError):
            headers = {"Content-Type": "application/json; charset=utf-8",
                       "Access-Token": "MOCK-ACCESS-TOKEN",
                       "Token-Type": "Bearer",
                       "Client": "MOCK-CLIENT",
                       "Expiry": "1000",
                       "Uid": "user@test.com"}

            responses.add(responses.POST, urljoin(COWBOY_URL, AUTH_ENDPOINT),
                          json=json.loads(golden.read()), status=200, headers=headers)
            auth = cowboybike.Authentication(
                "user@test.com", password="abcdef")
            accesstoken = auth.getaccesstoken()


@responses.activate
def test_expired_uid():
    with open('tests/goldenfiles/auth.json') as golden:
        with pytest.raises(ValueError):
            headers = {"Content-Type": "application/json; charset=utf-8",
                       "Access-Token": "MOCK-ACCESS-TOKEN",
                       "Token-Type": "Bearer",
                       "Client": "MOCK-CLIENT",
                       "Expiry": "1000",
                       "Uid": "user@test.com"}

            responses.add(responses.POST, urljoin(COWBOY_URL, AUTH_ENDPOINT),
                          json=json.loads(golden.read()), status=200, headers=headers)
            auth = cowboybike.Authentication(
                "user@test.com", password="abcdef")
            accesstoken = auth.getuid()


@responses.activate
def test_expired_client():
    with open('tests/goldenfiles/auth.json') as golden:
        with pytest.raises(ValueError):
            headers = {"Content-Type": "application/json; charset=utf-8",
                       "Access-Token": "MOCK-ACCESS-TOKEN",
                       "Token-Type": "Bearer",
                       "Client": "MOCK-CLIENT",
                       "Expiry": "1000",
                       "Uid": "user@test.com"}

            responses.add(responses.POST, urljoin(COWBOY_URL, AUTH_ENDPOINT),
                          json=json.loads(golden.read()), status=200, headers=headers)
            auth = cowboybike.Authentication(
                "user@test.com", password="abcdef")
            accesstoken = auth.getclient()


def test_get_request_missing_parameters():
    with pytest.raises(TypeError):
        cowboybike._getRequest()


@responses.activate
def test_get_request_missing_auth():
    with pytest.raises(ValueError):
        responses.add(responses.GET, urljoin(COWBOY_URL, CHECK_ENDPOINT),
                      json={}, status=200)

        cowboybike._getRequest(
            urljoin(COWBOY_URL, CHECK_ENDPOINT), authenticated=True)


@responses.activate
def test_get_valid_request():

    headers = {"Content-Type": "application/json; charset=utf-8",
               "Access-Token": "MOCK-ACCESS-TOKEN",
               "Token-Type": "Bearer",
               "Client": "MOCK-CLIENT",
               "Expiry": "1000",
               "Uid": "user@test.com"}

    responses.add(responses.GET, urljoin(COWBOY_URL, CHECK_ENDPOINT),
                  json={"key": "value"}, status=200, headers=headers)

    resp = cowboybike._getRequest(
        urljoin(COWBOY_URL, CHECK_ENDPOINT), authenticated=True, client="MOCK-CLIENT", uid="user@test.com", accesstoken="MOCK-ACCESS-TOKEN")

    assert resp["json"]["key"] == "value"
    assert resp["headers"]["Uid"] == "user@test.com"


@responses.activate
def test_get_request_404():
    responses.add(responses.GET, urljoin(COWBOY_URL, CHECK_ENDPOINT),
                  json={"key": "value"}, status=404)

    cowboybike._getRequest(
        urljoin(COWBOY_URL, CHECK_ENDPOINT))


@responses.activate
def test_get_request_no_json():
    responses.add(responses.GET, urljoin(COWBOY_URL, CHECK_ENDPOINT),
                  body="<html>MOCK</html>", status=200)

    cowboybike._getRequest(
        urljoin(COWBOY_URL, CHECK_ENDPOINT))


def test_post_request_missing_parameters():
    with pytest.raises(TypeError):
        cowboybike._postRequest()


@responses.activate
def test_post_request_missing_auth():
    with pytest.raises(ValueError):
        responses.add(responses.POST, urljoin(COWBOY_URL, CHECK_ENDPOINT),
                      json={}, status=200)

        cowboybike._postRequest(
            urljoin(COWBOY_URL, CHECK_ENDPOINT), authenticated=True)


@responses.activate
def test_post_valid_request():

    headers = {"Content-Type": "application/json; charset=utf-8",
               "Access-Token": "MOCK-ACCESS-TOKEN",
               "Token-Type": "Bearer",
               "Client": "MOCK-CLIENT",
               "Expiry": "1000",
               "Uid": "user@test.com"}

    responses.add(responses.POST, urljoin(COWBOY_URL, CHECK_ENDPOINT),
                  json={"key": "value"}, status=200, headers=headers)

    resp = cowboybike._postRequest(
        urljoin(COWBOY_URL, CHECK_ENDPOINT), data={"key": "value"}, authenticated=True, client="MOCK-CLIENT", uid="user@test.com", accesstoken="MOCK-ACCESS-TOKEN")

    assert resp["json"]["key"] == "value"
    assert resp["headers"]["Uid"] == "user@test.com"


@responses.activate
def test_post_request_404():
    responses.add(responses.POST, urljoin(COWBOY_URL, CHECK_ENDPOINT),
                  json={"key": "value"}, status=404)

    cowboybike._postRequest(
        urljoin(COWBOY_URL, CHECK_ENDPOINT))


@responses.activate
def test_post_request_no_json():
    responses.add(responses.POST, urljoin(COWBOY_URL, CHECK_ENDPOINT),
                  body="<html>MOCK</html>", status=200)

    cowboybike._postRequest(
        urljoin(COWBOY_URL, CHECK_ENDPOINT))


@responses.activate
def test_me_valid_request():

    headers = {"Content-Type": "application/json; charset=utf-8",
               "Access-Token": "MOCK-ACCESS-TOKEN",
               "Token-Type": "Bearer",
               "Client": "MOCK-CLIENT",
               "Expiry": str((datetime.now() + timedelta(days=365)).timestamp()),
               "Uid": "user@test.com"}

    with open('tests/goldenfiles/auth.json') as golden:
        json_resp = json.loads(golden.read())
        responses.add(responses.POST, urljoin(COWBOY_URL, AUTH_ENDPOINT),
                      json=json_resp, status=200, headers=headers)
        responses.add(responses.GET, urljoin(COWBOY_URL, ME_ENDPOINT),
                      json=json_resp, status=200, headers=headers)

    with open('tests/goldenfiles/bikes.json') as golden:
        json_resp = json.loads(golden.read())
        responses.add(responses.GET, urljoin(COWBOY_URL, BIKES_ENDPOINT),
                      json=json_resp, status=200, headers=headers)

        auth = cowboybike.Authentication(
            "user@test.com", password="abcdef")

        cowboy = cowboybike.Cowboy(auth)
        cowboy.refreshData()
